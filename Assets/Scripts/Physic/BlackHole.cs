﻿using UnityEngine;

namespace Physic
{
    public class BlackHole : MonoBehaviour
    {
        public Rigidbody mRigidbody;
    
        public float Gravity = 6.67f;
    
        void Attraction(BlackHole objToAttractor)
        {
            // Two objects need to have Rigidbody
            Rigidbody mRbToAttractor = objToAttractor.mRigidbody;
        
            // Direction
            Vector3 dir = mRigidbody.position - mRbToAttractor.position;
        
            // One Direction
            float distance = dir.magnitude;
        
            // Formula : F = G * ( (m1 * m2) / d * d )
            float forceMagnitude = Gravity * ((mRigidbody.mass * mRbToAttractor.mass) / Mathf.Pow(distance, 2));
        
            // Define Direction
            Vector3 force = dir.normalized * forceMagnitude;
        
            mRbToAttractor.AddForce(force);
        }
    
        void FixedUpdate()
        {
            // Select all planets
            BlackHole[] attractors = FindObjectsOfType<BlackHole>();

            foreach (BlackHole allPlanet in attractors)
            {
                if (allPlanet != this)
                {
                    Attraction(allPlanet);
                }
            }
        }
    
    }
}