﻿using UnityEngine;

namespace Physic
{
    public class AirResistance : MonoBehaviour
    {
        [SerializeField] private Rigidbody rb;
        [SerializeField] private float drag, angularDrag;
        
        void Start()
        {
            rb = GetComponent<Rigidbody>();
        }

        void FixedUpdate()
        {
            rb.drag = rb.velocity.magnitude * drag;
            rb.angularDrag = rb.velocity.magnitude * angularDrag;
        }
    }
}
