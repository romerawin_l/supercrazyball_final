﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Collections;

namespace Manager
{
    public class GameManager : MonoBehaviour
    {
        [SerializeField] private RectTransform startScreen;
        [SerializeField] private RectTransform endScreen;
        [SerializeField] private Button startButton;
        [SerializeField] private Button menuButton;
        [SerializeField] private Button quitMenuButton;
        [SerializeField] private Button quitButton2;
        [SerializeField] private Button restartDeathButton;
        [SerializeField] private Button restartDeath2Button;
        
        public void Awake()
        {
            Debug.Assert(startScreen != null,"startScreen can't be null");
            Debug.Assert(startButton != null,"startGame can't be null");
            Debug.Assert(menuButton != null,"menu can't be null");
            Debug.Assert(endScreen != null,"endScreen can't be null");
            Debug.Assert(quitMenuButton != null, "quitMenuButton can't be null");
            Debug.Assert(quitButton2 != null, "quitButton2 can't be null");
            Debug.Assert(restartDeathButton != null,"restartDeathButton can't null");
            Debug.Assert(restartDeath2Button != null,"restartDeath2Button can't null");

            restartDeathButton.onClick.AddListener(OnRestartButtonClicked);
            startButton.onClick.AddListener(OnStartButtonClicked);
            quitMenuButton.onClick.AddListener(OnQuitButtonClicked);
            quitButton2.onClick.AddListener(OnQuitButton2Clicked);
            menuButton.onClick.AddListener(OnMenuButtonButtonClicked);
            restartDeath2Button.onClick.AddListener(OnRestartStageTwoButtonClicked);
        }

        private void OnStartButtonClicked()
        {
            SceneManager.LoadScene("FirstStage");
        }
        
        private void OnQuitButtonClicked()
        {
            Application.Quit();
        }
        
        private void OnQuitButton2Clicked()
        {
            Application.Quit();
        }

        private void OnMenuButtonButtonClicked()
        {
            SceneManager.LoadScene("FirstStage");
        }
        
        private void OnRestartStageTwoButtonClicked()
        {
            SceneManager.LoadScene("SecondStage");
        }
        
        private void OnRestartButtonClicked()
        {
            SceneManager.LoadScene("FirstStage");
        }
    }
}
