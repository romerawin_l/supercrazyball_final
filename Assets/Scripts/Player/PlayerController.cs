﻿using UnityEngine;

namespace Player
{
    public class PlayerController : MonoBehaviour
    {
        [SerializeField] private Rigidbody rb;

        public float speed;
        public float jumpForce;

        private void Start()
        {
            rb = GetComponent<Rigidbody>();
        }

        private void FixedUpdate()
        {
            float moveHorizontal = Input.GetAxis("Horizontal");
            float moveVertical = Input.GetAxis("Vertical");

            Vector3 movement = new Vector3(-moveHorizontal, 0, -moveVertical);
            rb.AddForce(movement * speed);

            bool jump = Input.GetButton("Jump");
            bool quit = Input.GetKey(KeyCode.Escape);

            if (jump)
            {
                rb.AddForce(new Vector3(0,jumpForce,0),ForceMode.Impulse);
            }

            if (quit)
            {
                Application.Quit();
            }
        }

    }
}
