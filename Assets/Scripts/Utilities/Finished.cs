﻿using UnityEngine.UI;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Utilities
{
    public class Finished : MonoBehaviour
    {
        [SerializeField] private RectTransform stageScreen;
        [SerializeField] private Button nextStage;
        [SerializeField] private Button menuButton;


        protected void Awake()
        {
            Debug.Assert(stageScreen != null,"stageScreen can't be null");
            Debug.Assert(nextStage != null, "nextStage can't be null");
            Debug.Assert(menuButton != null, "menuButton can't be null");

            
            nextStage.onClick.AddListener(OnNextStageButtonClicked);
            menuButton.onClick.AddListener(OnMenuButtonClicked);
        }

        private void OnTriggerEnter(Collider other)
        {
            stageScreen.gameObject.SetActive(true);
            GameObject.Find("Player").SendMessage("Finish");
            Destroy(GameObject.Find("Player"));
        }
        
        private void OnNextStageButtonClicked()
        {
            SceneManager.LoadScene("SecondStage");
        }
        
        private void OnMenuButtonClicked()
        {
            SceneManager.LoadScene("Menu");
        }
    }
}
