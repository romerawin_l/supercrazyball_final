﻿using UnityEngine;
using UnityEngine.UI;

namespace Utilities
{
    public class Timer : MonoBehaviour
    {
        public Text timerText;
        private float _startTime;
        private bool _finished = false;
        
        void Start()
        {
            _startTime = Time.time;
        }

        void Update()
        {
            if (_finished)
            {
                return;
            }
            
            float t = Time.time - _startTime;
            string minutes = ((int) t / 60).ToString();
            string seconds = (t % 60).ToString("f2");
            timerText.text = minutes + ":" + seconds;
        }

        public void Finish()
        {
            _finished = true;
            timerText.color = Color.cyan;
        }
    }
}
