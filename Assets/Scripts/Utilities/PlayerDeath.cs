﻿using UnityEngine;

namespace Utilities
{
    public class PlayerDeath : MonoBehaviour
    {
        [SerializeField] private RectTransform deathScreen;
        
        protected void Awake()
        {
            Debug.Assert(deathScreen != null,"deathScreen can't be null");
        }
        
        private void OnCollisionEnter(Collision other)
        {
            if (other.gameObject.CompareTag("Player"))
            { 
                deathScreen.gameObject.SetActive(true);
                Destroy(GameObject.Find("Player"));
            }
        }
    }
}
